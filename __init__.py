#!/usr/bin/env python3

"""Mon docstring"""
import sqlite3
import re
from flask import Flask, request, redirect, render_template, session, flash, url_for

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


CONN = sqlite3.connect('best_project.db', check_same_thread=False) #pabien
C = CONN.cursor()
# pas de date de naissance dans la base de donnée
C.execute("""
    CREATE TABLE IF NOT EXISTS User (
        idUser INTEGER PRIMARY KEY AUTOINCREMENT, 
        User_name VARCHAR(20) NOT NULL, 
        User_surname VARCHAR(20) NOT NULL, 
        User_pseudo VARCHAR(20) NOT NULL, 
        User_password VARCHAR(20) NOT NULL, 
        User_email VARCHAR(20) NOT NULL, 
        User_phone VARCHAR(20) NOT NULL,
        Maths INTEGER NOT NULL,
        Francais INTEGER NOT NULL,
        Histoire INTEGER NOT NULL,
        Chimie INTEGER NOT NULL,
        Philo INTEGER NOT NULL,
        Anglais INTEGER NOT NULL,
        Physique INTERGER NOT NULL,
        Points INTEGER)
    """)

def create_app():
    """ On créer notre fonction pour l'appeler dans server.py"""
    app = Flask(__name__, static_folder="public", static_url_path="")
    app.secret_key = b'best_project'

#pages de bases

    @app.route("/")
    def home():
        return redirect('/page_accueil.html')

    @app.route("/login")
    def login():
        if 'connexion_ok' in session:
            session.pop("connexion_ok", None)
            flash("Vous êtes déconecté")
            return redirect('/')
        else:
            return redirect('/connexion.html')


    @app.route("/nv_mdp", methods=["GET", "POST"])
    def nv_mdp_save():
        nv_mdp=request.form["nv_mdp"]
        print(nv_mdp)
        if (re.compile("[a-z]").search(nv_mdp)):
            print("min_ok")
            if (re.compile("[A-Z]").search(nv_mdp)):
                print("maj_ok")
                if (re.compile("[0-9]").search(nv_mdp)):
                    print("chiffre_ok")
                    if (len(nv_mdp) > 7):
                        id_user = session['connexion_ok']
                        print("taille_ok")
                        C.execute("""UPDATE User SET User_password = ? WHERE idUser = ? """, (nv_mdp, id_user))
                        CONN.commit()
                        return redirect('/')
        else :
            print("Votre mot de passe doit contenir une minuscule, une majuscule, un nombre et faire plus de 8 caractère")
            return redirect('/test')


    @app.route("/verif_co", methods=["GET", "POST"])
    def verif_co():
        C.execute("""SELECT idUser, User_pseudo, User_password FROM User""")
        pseudo = request.form["pseudo"]
        mdp = request.form["mdp"]
        print(pseudo)
        for row in C:
            print(row[1])
            if row[1] == pseudo:
                print(row[2])
                if row[2] == mdp:
                    id_user = row[0]
                    session['connexion_ok'] = id_user
                    print("vous etes connecté")  #faire un pop up avec "vous etes connecté"
                    print(id_user)
                    return redirect('/')
                else:
                    print("mdp incorrect")
            else:
                print("pseudo incorrect")
        return redirect('/connexion.html')


    @app.route("/mon_profil", methods=["GET"])
    def mon_profil_co():
        if 'connexion_ok' in session:
            n_utilisateur = session['connexion_ok']            
            query_string = "SELECT * FROM User WHERE idUser = ?"
            rows = C.execute(query_string, (n_utilisateur,))
            rv = rows.fetchall()
            info = render_template('/mon_profil.html', mon_profil=rv)
            return info

        else:
            return redirect('/connexion.html')


# recherche de cours

    @app.route("/recherche_cours")
    def recherche_cours_co():
        if 'connexion_ok' in session:
            return redirect('/TrouverUnCours.html')

        else:
            return redirect('/connexion.html')

    @app.route("/contact_prof", methods=["GET", "POST"])
    def contact_prof():
        id_prof = request.form["id_prof"]
        email_prof_temp = C.execute("SELECT User_email FROM User WHERE idUser = ?", (id_prof,)).fetchall()
        email_prof = email_prof_temp[0][0]
        points_prof_temp = C.execute("SELECT Points FROM User WHERE idUser = ?", (id_prof,)).fetchall()
        points_prof = points_prof_temp[0][0] + 10
        C.execute("""UPDATE User SET Points = ? WHERE idUser = ? """, (points_prof, id_prof))
        CONN.commit()

        id_student = session['connexion_ok']
        row = C.execute("SELECT * FROM User WHERE idUser = ?", (id_student,)).fetchall()
        points_eleve = row[0][14] - 10
        C.execute("""UPDATE User SET Points = ? WHERE idUser = ? """, (points_eleve, id_student))
        CONN.commit() 

        nom = row[0][1]
        prenom = row[0][2]
        tel = row[0][6]
        mail = row[0][5]
        print(email_prof)
        msg = MIMEMultipart()
        msg['From'] = 'teach.and.learn.mail@gmail.com'  #l'adresse mail du site
        msg['To'] = email_prof
        msg['Subject'] = 'vous avez une demande de cours'
        message = "\n Bonjour ! \n{} {} souhaiterez avoir un cours avec vous, contactez le au {} ou via {}".format(nom, prenom, tel, mail)
        msg.attach(MIMEText(message))
        mailserver = smtplib.SMTP('smtp.gmail.com', 587)
        mailserver.ehlo()
        mailserver.starttls()
        mailserver.ehlo()
        mailserver.login('teach.and.learn.mail@gmail.com', 'Teach_And_Learn1')
        mailserver.sendmail('teach.and.learn.mail@gmail.com', email_prof, msg.as_string())
        mailserver.quit()
        return redirect("/")


#selectionne les cours de bon niveau

    @app.route("/cours_francais", methods=["GET", "POST"])
    def cours_francais():
        nv_francais = request.form["niveau_cours_francais"]
        if 'connexion_ok' in session:
            rows = C.execute("SELECT * FROM User WHERE Francais > " + nv_francais)
            resultats_fr = render_template('/cours_francais.html', cours_francais=rows)
            return resultats_fr

        else:
            return redirect("/connexion.html")

    @app.route("/cours_maths", methods=["GET", "POST"])
    def cours_maths():
        nv_maths = request.form["niveau_cours_maths"]
        if 'connexion_ok' in session:
            print(nv_maths)
            rows = C.execute("SELECT * FROM User WHERE Maths > " + nv_maths)
            resultats_mth = render_template('/cours_maths.html', cours_maths=rows)
            return resultats_mth

        else:
            return redirect("/connexion.html")

    @app.route("/cours_histoire", methods=["GET", "POST"])
    def cours_histoire():
        nv_histoire = request.form["niveau_cours_histoire"]
        if 'connexion_ok' in session:
            rows = C.execute("SELECT * FROM User WHERE Histoire > " + nv_histoire)
            resultats_his = render_template('/cours_histoire.html', cours_histoire=rows)
            return resultats_his

        else:
            return redirect("/connexion.html")

    @app.route("/cours_chimie", methods=["GET", "POST"])
    def cours_chimie():
        nv_chimie = request.form["niveau_cours_chimie"]
        if 'connexion_ok' in session:
            rows = C.execute("SELECT * FROM User WHERE Chimie > " + nv_chimie)
            resultats_ch = render_template('/cours_chimie.html', cours_chimie=rows)
            return resultats_ch

        else:
            return redirect("/connexion.html")

    @app.route("/cours_philo", methods=["GET", "POST"])
    def cours_philo():
        nv_philo = request.form["niveau_cours_philo"]
        if 'connexion_ok' in session:
            rows = C.execute("SELECT * FROM User WHERE Philo > " + nv_philo)
            resultats_ph = render_template('/cours_philo.html', cours_philo=rows)
            return resultats_ph

        else:
            return redirect("/connexion.html")

    @app.route("/cours_anglais", methods=["GET", "POST"])
    def cours_anglais():
        nv_anglais = request.form["niveau_cours_anglais"]
        if 'connexion_ok' in session:
            rows = C.execute("SELECT * FROM User WHERE Anglais > " + nv_anglais)
            resultats_an = render_template('/cours_anglais.html', cours_anglais=rows)
            return resultats_an

        else:
            return redirect("/connexion.html")

    @app.route("/cours_physique", methods=["GET", "POST"])
    def cours_physique():
        nv_physique = request.form["niveau_cours_physique"]
        if 'connexion_ok' in session:
            rows = C.execute("SELECT * FROM User WHERE Physique > " + nv_physique)
            resultats_phy = render_template('/cours_physique.html', cours_physique=rows)
            return resultats_phy

        else:
            return redirect("/connexion.html")



    @app.route("/inscription", methods=["POST"])
    def inscription():
        prenom = request.form["prenom"]
        nom = request.form["nom"]
        tel = request.form["tel"]
        pseudo = request.form["pseudo"]
        mdp = request.form["mdp"]
        email = request.form["email"]
#        date = request.form["date"]
        math = request.form["niveau_maths"]
        francais = request.form["niveau_francais"]
        histoire = request.form["niveau_histoire"]
        chimie = request.form["niveau_chimie"]
        physique = request.form["niveau_physique"]
        anglais = request.form["niveau_anglais"]
        philo = request.form["niveau_philo"]

        C.execute("INSERT INTO User \
                (User_name, User_surname, User_pseudo, User_password, User_email, User_phone, Maths, \
                Francais, Histoire, Chimie, Philo, Anglais, Physique, Points) \
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", \
                (nom, prenom, pseudo, mdp, email, tel, math, francais, histoire, chimie, philo, anglais, physique, 10))
        CONN.commit()

        return redirect('/page_accueil.html')

    return app

#app.run('127.0.0.1')
